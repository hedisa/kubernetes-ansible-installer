<h1 align="center">Welcome to Kubernetes-ansible-installer 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Deploying a Kubernetes cluster with Ansible
## Prerequisites

-   3 VMS (One master node and two worker nodes).

-   2 GB or more of RAM per machine.

-   2 CPUs or more.

-   Full network connectivity between all machines in the cluster.
    
-   Ansible installed on your local machine.
    
-   A user (ansible) inside all vms with passwordless sudo privilege.

## Usage

1.  Specify servers IP addresses in the hosts file.

2.  Run playbooks using the command below:

```sh
ansible-playbook -i hosts install-kube.yml
```

## Author

👤 **Hedi Saaidi**

* Website: https://hedisaaidi.me
* Gitlab: [@hedisa](https://github.com/hedisa)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/hedisa/kubernetes-ansible-installer/-/issues). 

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_